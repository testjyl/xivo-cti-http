# -*- coding: UTF-8 -*-

# Copyright (C) 2012  Avencall
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA..

from xivo_client.client.step import ConnectStep, LoginStep, PasswordStep, \
    CapasStep, IPBXListStep, SubscribeToQueuesStatsStep, InitializationStep, \
    LogoutStep, StepRunner
import logging
from xivo_cti_http.services.queue_stat_step import QueueStatStep

logger = logging.getLogger(__name__)


class CTIHttpScenario(object):

    def __init__(self, client, username, password, queue_stat_service):

        steps = [
              ConnectStep(),
              LoginStep(),
              PasswordStep(),
              CapasStep(),
              IPBXListStep(),
              SubscribeToQueuesStatsStep(),
              InitializationStep(),
              QueueStatStep(client, queue_stat_service),
              LogoutStep()
              ]
        self._runner = StepRunner(client, steps, {'username': username, 'password': password})

    def run(self):
        self._runner.run()
