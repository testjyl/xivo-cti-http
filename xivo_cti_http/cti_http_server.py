# -*- coding: UTF-8 -*-

from __future__ import absolute_import

import logging
from xivo_cti_http.http_server import HttpServer
import gevent
from xivo_cti_http.services.queue_stat_service import QueueStatService
from xivo_cti_http.services.queue_info_manager import QueueInfoManager
from xivo_cti_http.cti_http_scenario import CTIHttpScenario
from xivo_cti_http.cti_client import CtiClient
from xivo_cti_http.application.callme_application import CallMeApplication
from xivo_cti_http.application.call_me_content_builder import CallMeContentBuilder
from xivo_cti_http.application.file_writer import FileWriter
from xivo_cti_http.application import conf

logger = logging.getLogger(__name__)


class CTIHttpServer(object):

    MESSAGE_GETLIST = "getlist"

    def __init__(self):
        self._queue_info_manager = QueueInfoManager()
        self._queue_stat_service = QueueStatService(self._queue_info_manager)

        self._callback_application = CallMeApplication(CallMeContentBuilder(), FileWriter())

        self._cti_username = conf.xivo_cti_http_config['cti']['username']
        self._cti_password = conf.xivo_cti_http_config['cti']['password']

    def run(self):
        logger.info('http cti service starting on HTTP port number %s', conf.xivo_cti_http_config['http']['port'])

        http_server = HttpServer(int(conf.xivo_cti_http_config['http']['port']), self._queue_stat_service, self._callback_application)
        server_greenlet = gevent.spawn(http_server.run)
        cti_greenlet = self.create_cti_greenlet(conf.xivo_cti_http_config['cti']['server_address'], int(conf.xivo_cti_http_config['cti']['port']), self._queue_stat_service)
        gevent.joinall([cti_greenlet, server_greenlet])
        logger.info('http cti service stopped .....')

    def create_cti_greenlet(self, hostname, port_number, queue_stat_service):
        client = CtiClient(hostname, port=port_number)
        client.register(self._queue_info_manager.on_queue_config, self.MESSAGE_GETLIST)
        scenario = CTIHttpScenario(client, self._cti_username, self._cti_password, queue_stat_service)
        cti_greenlet = gevent.spawn(scenario.run)
        return cti_greenlet
