# -*- coding: UTF-8 -*-

# Copyright (C) 2012  Avencall
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA..


from xivo_client.client.cti import CTIClient
import logging
import itertools

logger = logging.getLogger(__name__)


class CtiClient(CTIClient):

    def __init__(self, host, port):
        CTIClient.__init__(self, host, port)
        self.command_id = itertools.count(1)
        self._messages_class_processors = {}

    def get_queue_info(self, queue_id):
        logging.info("requesting info for queue %s", queue_id)
        message = {"class": "getlist",
                    "commandid": 999 + self.command_id.next(),
                    "function": "updateconfig",
                    "listname": "queues",
                    "tid": queue_id,
                    "tipbxid": "xivo"}

        self.send_msg(message)

    def register(self, method, message_class):
        self._messages_class_processors[message_class] = method

    def on_message_received(self, message):
        try:
            self._messages_class_processors[message["class"]](message)
        except KeyError:
            pass

    def recv_msg_matching_class(self, msg_class):
        # warning: discard all other messages
        while True:
            msg = self.recv_msg()
            if msg['class'] == msg_class:
                return msg
            else:
                self.on_message_received(msg)
