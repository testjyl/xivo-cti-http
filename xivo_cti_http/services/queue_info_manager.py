# -*- coding: UTF-8 -*-

# Copyright (C) 2012  Avencall
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA..
import logging

logger = logging.getLogger(__name__)


class QueueInfoNotFound(Exception):

    def __init__(self, queue_id):
        self.queue_id = queue_id


class QueueInfoManager(object):

    def __init__(self):
        self._queue_configs = {}

    def queue_exists(self, queue_id):
        return queue_id in self._queue_configs

    def get_queue_id(self, queue_number):
        for queue_id in self._queue_configs:
            if self._queue_configs[queue_id] == queue_number:
                return queue_id

    def on_queue_config(self, message):
        logger.info("Message :%s", message)
        queue_id = message['tid']
        queue_number = message['config']['number']
        self._queue_configs[queue_id] = queue_number
