# -*- coding: UTF-8 -*-

# Copyright (C) 2012  Avencall
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA..
from xivo_cti_http.services.queue_info_manager import QueueInfoNotFound


class QueueStatStep(object):

    MESSAGE_CLASS = "getqueuesstats"

    def __init__(self, cti_client, queue_stat_service):
        self._queue_stat_service = queue_stat_service
        self._cti_client = cti_client

    def run(self, runner):
        while(True):
            self.process_statistics()

    def process_statistics(self):
        statistics = self._cti_client.recv_msg_matching_class(self.MESSAGE_CLASS)
        try:
            self._queue_stat_service.stat_received(statistics)
        except QueueInfoNotFound as queue_info_not_found:
            self._cti_client.get_queue_info(queue_info_not_found.queue_id)
