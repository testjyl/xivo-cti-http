# -*- coding: UTF-8 -*-

# Copyright (C) 2012  Avencall
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA..

import unittest
from mock import Mock
from xivo_cti_http.services.queue_stat_service import QueueStatService
from xivo_cti_http.services.queue_stat_step import QueueStatStep
from xivo_cti_http.services.queue_info_manager import QueueInfoNotFound
from xivo_cti_http.cti_client import CtiClient


class TestQueueStatStep(unittest.TestCase):

    def setUp(self):
        self._queue_stat_service = Mock(QueueStatService)
        self._cti_client = Mock(CtiClient)
        self._queue_stat_step = QueueStatStep(self._cti_client, self._queue_stat_service)

    def tearDown(self):
        pass

    def test_process_statistics(self):
        statistics = {}
        self._cti_client.recv_msg_matching_class.return_value = statistics

        self._queue_stat_step.process_statistics()

        self._cti_client.recv_msg_matching_class.assert_called_with(QueueStatStep.MESSAGE_CLASS)
        self._queue_stat_service.stat_received.assert_called_with(statistics)

    def test_process_statistics_queue_not_found(self):
        statistics = {}
        queue_id = '45'
        queue_info_not_found = QueueInfoNotFound(queue_id)
        self._queue_stat_service.stat_received.side_effect = queue_info_not_found
        self._cti_client.recv_msg_matching_class.return_value = statistics

        self._queue_stat_step.process_statistics()

        self._cti_client.recv_msg_matching_class.assert_called_with(QueueStatStep.MESSAGE_CLASS)

        self._cti_client.get_queue_info.assert_called_with(queue_id)
