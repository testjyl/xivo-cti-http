# -*- coding: UTF-8 -*-

# Copyright (C) 2012  Avencall
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA..
import unittest
from xivo_cti_http.services.queue_info_manager import QueueInfoManager


class TestQueueInfoManager(unittest.TestCase):

    def setUp(self):
        self._queue_info_manager = QueueInfoManager()

    def test_on_queue_config(self):
        queue_id = '4'
        message = {u'function': u'updateconfig',
                   u'listname': u'queues',
                   u'tipbxid': u'xivo',
                   u'timenow': 1346860322.18,
                   u'tid': queue_id, u'config':
                        {u'displayname': u'Expert\xe9s',
                         u'name': u'expert',
                         u'context': u'default',
                         u'number': u'5003'},
                   u'class': u'getlist'}
        self._queue_info_manager.on_queue_config(message)

        self.assertTrue(self._queue_info_manager.queue_exists(queue_id))

    def test_get_queue_id(self):
        queue_id = '4'
        message = {u'function': u'updateconfig',
                   u'listname': u'queues',
                   u'tipbxid': u'xivo',
                   u'timenow': 1346860322.18,
                   u'tid': queue_id, u'config':
                        {u'displayname': u'Expert\xe9s',
                         u'name': u'expert',
                         u'context': u'default',
                         u'number': u'5003'},
                   u'class': u'getlist'}
        self._queue_info_manager.on_queue_config(message)

        queue_id = self._queue_info_manager.get_queue_id('5003')

        self.assertEquals(queue_id, '4', "unable to retreive queue id from number")


