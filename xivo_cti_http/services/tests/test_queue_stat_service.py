# -*- coding: UTF-8 -*-

# Copyright (C) 2012  Avencall
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA..

import unittest
from xivo_cti_http.services.queue_stat_service import QueueStatService
from mock import Mock, patch
from xivo_cti_http.services.queue_info_manager import QueueInfoManager, \
    QueueInfoNotFound


mock_get_ewt = Mock()


class TestQueueStatService(unittest.TestCase):

    def setUp(self):
        self._queue_info_manager = Mock(QueueInfoManager)
        self._queue_stat_service = QueueStatService(self._queue_info_manager)

    def test_receive_stat_no_ewt(self):
        queue_number = '4'
        statistic = {u'stats':
                        {queue_number:
                         {u'Xivo-WaitingCalls': u'3'}
                         },
                     u'class': u'getqueuesstats', u'timenow': 1346771555.59}

        self._queue_stat_service.stat_received(statistic)

        self.assertTrue(True, 'This statistic should be ignored')

    def test_receive_stat_queue_info_exists(self):
        queue_id = '4'
        statistic = {u'stats':
                        {queue_id:
                         {u'Xivo-EWT': u'14'}
                         },
                     u'class': u'getqueuesstats', u'timenow': 1346771555.59}

        self._queue_info_manager.queue_exists.return_value = True

        self._queue_stat_service.stat_received(statistic)

        self._queue_info_manager.queue_exists.assert_called_with('4')

    def test_receive_stat_queue_info_doesnt_exists(self):
        queue_id = '4'
        statistic = {u'stats':
                        {queue_id:
                         {u'Xivo-EWT': u'27'}
                         },
                     u'class': u'getqueuesstats', u'timenow': 1346771555.59}

        self._queue_info_manager.queue_exists.return_value = False

        try:
            self._queue_stat_service.stat_received(statistic)
        except QueueInfoNotFound as queue_info_not_found:
            self.assertEqual(queue_info_not_found.queue_id, queue_id, "Invalid id in exception")

        self._queue_info_manager.queue_exists.assert_called_with('4')
        self.assertEquals(27, self._queue_stat_service.get_ewt(queue_id))

    def test_get_ewt(self):
        queue_id = '5003'
        statistic = {u'stats':
                        {queue_id:
                         {u'Xivo-EWT': u'14'}
                         },
                     u'class': u'getqueuesstats', u'timenow': 1346771555.59}

        self._queue_stat_service.stat_received(statistic)

        ewt = self._queue_stat_service.get_ewt(queue_id)
        self.assertEqual(14, ewt)

    @patch.object(QueueStatService, 'get_ewt', mock_get_ewt)
    def test_get_ewt_from_number(self):
        queue_number = '5003'
        queue_id = '4'
        mock_get_ewt.return_value = 45
        self._queue_info_manager.get_queue_id.return_value = queue_id

        ewt = self._queue_stat_service.get_ewt_from_number(queue_number)

        self._queue_info_manager.get_queue_id.assert_called_with(queue_number)
        mock_get_ewt.assert_called_with(queue_id)
        self.assertEquals(ewt, 45)

    def test_get_ewt_no_value(self):
        ewt = self._queue_stat_service.get_ewt('27')

        self.assertEqual(0, ewt)

    def test_get_ewt_available_agents(self):
        queue_number = '45'
        statistic = {u'stats':
                        {queue_number:
                         {u'Xivo-AvailableAgents': u'3', u'Xivo-EWT': u'14'}
                         },
                     u'class': u'getqueuesstats', u'timenow': 1346771555.59}

        self._queue_stat_service.stat_received(statistic)

        ewt = self._queue_stat_service.get_ewt('45')

        self.assertEqual(0, ewt)
