# -*- coding: UTF-8 -*-

# Copyright (C) 2012  Avencall
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA..

import logging
from xivo_cti_http.services.queue_info_manager import QueueInfoNotFound

logger = logging.getLogger(__name__)


class QueueStatService(object):

    queue_statistics = {}

    def __init__(self, queue_info_manager):
        self._queue_info_manager = queue_info_manager

    def stat_received(self, statistic):
        logger.info(statistic)
        queue_id = statistic['stats'].keys()[0]
        if queue_id not in self.queue_statistics:
            self.queue_statistics[queue_id] = {}
        try:
            ewt = statistic['stats'][queue_id]['Xivo-EWT']
            self.queue_statistics[queue_id]['Xivo-EWT'] = int(ewt)
        except KeyError:
            pass
        try:
            available_agents = statistic['stats'][queue_id]['Xivo-AvailableAgents']
            self.queue_statistics[queue_id]['Xivo-AvailableAgents'] = int(available_agents)
        except KeyError:
            pass

        if not self._queue_info_manager.queue_exists(queue_id):
            raise(QueueInfoNotFound(queue_id))

    def get_ewt_from_number(self, queue_number):
        logger.info("queue number : %s", queue_number)
        queue_id = self._queue_info_manager.get_queue_id(queue_number)
        return self.get_ewt(queue_id)

    def get_ewt(self, queue_id):
        logger.info("queue id : %s", queue_id)
        try:
            print self.queue_statistics[queue_id]
            if 'Xivo-AvailableAgents' not in self.queue_statistics[queue_id] or self.queue_statistics[queue_id]['Xivo-AvailableAgents'] == 0:
                return self.queue_statistics[queue_id]['Xivo-EWT']
            else:
                return 0
        except KeyError:
            logger.info('no ewt for queue : %s', queue_id)
            return 0

    def dummy(self):
        pass
