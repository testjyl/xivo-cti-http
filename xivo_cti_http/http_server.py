# -*- coding: UTF-8 -*-

# Copyright (C) 2012  Avencall
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA..
from gevent.wsgi import WSGIServer
import logging
import urlparse

logger = logging.getLogger(__name__)


class HttpServer(object):

    def __init__(self, port_number, queue_stat_service, callback_application):
        self._port_number = port_number
        self._queue_stat_service = queue_stat_service
        self._callback_application = callback_application

    def application(self, environ, start_response):
        logger.info(environ)
        logger.info(urlparse.parse_qs(environ['QUERY_STRING']))
        status = '200 OK'
        headers = [
            ('Content-Type', 'application/json')
        ]
        start_response(status, headers)

        if environ['PATH_INFO'] == '/callme/':
            return self._callback_application.process(urlparse.parse_qs(environ['QUERY_STRING']))
        else:
            ewt = '{"EWT": ' + str(self._queue_stat_service.get_ewt_from_number(environ['QUERY_STRING'])) + '}'
            return ewt

    def run(self):
        self.server = WSGIServer(('', self._port_number), self.application)
        self.server.serve_forever()
