# -*- coding: UTF-8 -*-

# Copyright (C) 2012  Avencall
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA..


import logging
import ConfigParser

logger = logging.getLogger('config')
CONFIG_FILE = "/etc/xivo-cti-http.conf"

xivo_cti_http_config = ""
xivo_cti_http_config = {'cti': {
                                'username': 'xivo_cti',
                                'password': 'xivo_cti',
                                'server_address': '127.0.0.1',
                                'port': '5003'
                                },
                        'callme': {
                                   'source_path': '/var/spool/asterisk/tmp/',
                                   'destination_path': '/var/spool/asterisk/outgoing/',
                                   },
                        'http': {
                                 'port': '5000'
                                 }
                        }


def readConf(configFile=CONFIG_FILE):
    logger.info('--- Reading config file:' + configFile)
    config_parser = ConfigParser.ConfigParser()
    config_parser.read(configFile)

    for section in config_parser.sections():
        logger.info("----------" + section + " configuration --------------")
        for (name, value) in config_parser.items(section):
            logger.info(name + ": " + value)
            xivo_cti_http_config[section][name] = value
        logger.info("")
