# -*- coding: UTF-8 -*-
from xivo_cti_http.application.conf import xivo_cti_http_config

# Copyright (C) 2012  Avencall
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA..



class CallMeApplication(object):

    def __init__(self, call_me_content_builder, writer):
        self._dest_path = xivo_cti_http_config['callme']['destination_path']
        self._call_me_content_builder = call_me_content_builder
        self._writer = writer

    def set_dest_path(self, dest_path):
        self._dest_path = dest_path

    def process(self, query):
        callme_id = query['id'][0]
        content = self._call_me_content_builder.build(callme_id, query['id_ars'][0], query['queue_nb'][0], query['nb'][0])
        self._writer.write(callme_id, content, self._dest_path)
        return ('{"status": "OK"}')
