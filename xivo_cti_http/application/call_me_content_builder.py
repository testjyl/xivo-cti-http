# -*- coding: UTF-8 -*-

# Copyright (C) 2012  Avencall
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA..


class CallMeContentBuilder(object):

    FILE_TEMPLATE = """\
Channel: Local/<queue_nb>@default
MaxRetries: 8
RetryTime: 900
WaitTime: 30
Setvar: ID_ARS=<id_ars>
Context: rappel-utilisateur
Extension: <nb>
Priority: 1
CallerID: <id_callme>
"""

    def build(self, id_callme, id_ars, queue_nb, nb):
        self._output = self.FILE_TEMPLATE

        method_params = locals().keys()
        for method_param in method_params:
            if method_param != "self":
                self._output = self._output.replace("<" + method_param + ">", locals()[method_param])

        return self._output
