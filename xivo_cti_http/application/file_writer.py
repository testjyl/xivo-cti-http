# -*- coding: UTF-8 -*-

# Copyright (C) 2012  Avencall
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA..
import io
import os
import logging
from xivo_cti_http.application.conf import xivo_cti_http_config

logger = logging.getLogger(__name__)


class FileWriter():

    FILE_ROOT = "callme"

    def __init__(self):
        self._source_path = xivo_cti_http_config['callme']['source_path']

    def write(self, callme_id, content, dest_path):
        file_name = self.FILE_ROOT + str(callme_id)
        callMeFile = io.FileIO(self._source_path + file_name, mode='w', closefd=True)
        callMeFile.write(content)
        callMeFile.close()
        logger.info('rename src:' + self._source_path + file_name + ', \ndst:' + dest_path + file_name)
        os.rename(self._source_path + file_name, dest_path + file_name)
