# -*- coding: UTF-8 -*-

# Copyright (C) 2012  Avencall
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA..


import unittest
import io
import os
from xivo_cti_http.application.file_writer import FileWriter
import random
import time


class TestFileWriter(unittest.TestCase):

    FILE_PATH = "./"
    FILE_ROOT = "callme"
    DST_PATH = "./spool/"

    FILE_NAME_TEMPLATE = FILE_PATH + FILE_ROOT

    def setUp(self):
        if not os.path.exists(self.DST_PATH):
            os.mkdir(self.DST_PATH)
        self._removeOldCallMeFiles()
        self._file_writer = FileWriter()
        self._file_writer._source_path = self.FILE_PATH

    def testFileContentWriter(self):
        expected = "Toto" + str(random.randint(1, 1000)) + str(time.time())
        callme_id = str(random.randint(1000, 9999))

        self._file_writer.write(callme_id, expected, self.DST_PATH)

        self.assertFileWritten(callme_id, expected, self.DST_PATH)

    def assertFileWritten(self, callme_id, expected, dstPath):
        self._callMeFile = io.FileIO(dstPath + self.FILE_NAME_TEMPLATE + str(callme_id), mode='r', closefd=True)
        content = self._callMeFile.read()
        self.assertEqual(expected, str(content))

    def _removeOldCallMeFiles(self):
        self.assertGreater(len(self.DST_PATH), 4, "to protect mis usage of remove all files")
        allfiles = os.listdir(self.DST_PATH)
        callme_files = [file_name for file_name in allfiles if file_name.find(self.FILE_ROOT) == 0]
        for callme_file in callme_files:
            try:
                os.remove(callme_file)
            except OSError as osError:
                if osError.errno != 2:
                    raise osError
