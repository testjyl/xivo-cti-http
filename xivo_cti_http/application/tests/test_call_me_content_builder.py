# -*- coding: UTF-8 -*-

# Copyright (C) 2012  Avencall
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA..


import unittest
from xivo_cti_http.application.call_me_content_builder import CallMeContentBuilder


class TestCallMeContentBuilder(unittest.TestCase):

    def setUp(self):
        self._call_me_content_builder = CallMeContentBuilder()

    def test_build(self):

        expected = """\
                    Channel: Local/5006@default
                    MaxRetries: 8
                    RetryTime: 900
                    WaitTime: 30
                    Setvar: ID_ARS=5278
                    Context: rappel-utilisateur
                    Extension: 0298143388
                    Priority: 1
                    CallerID: 456
                   """

        content = self._call_me_content_builder.build('456', '5278', '5006', '0298143388')

        self.assertContentEqual(expected, content)

    def assertContentEqual(self, configExpected, configResult):
        self.assertEqual(configExpected.replace(' ', ''), configResult.replace(' ', ''))
