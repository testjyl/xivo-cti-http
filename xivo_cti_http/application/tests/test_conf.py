# -*- coding: UTF-8 -*-

# Copyright (C) 2012  Avencall
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA..

import unittest
import io
from xivo_cti_http.application import conf
import random
import os
import glob


class TestConf(unittest.TestCase):

    FILE_ROOT = './testConfig'
    
    def setUp(self):

        self._config_file_name=self.FILE_ROOT + str(random.randint(100,999))
        
        self.removeOldTestConfFiles()
        
        self._variable_content = {'cti': {
                                'username': 'testuser' + str(random.randint(1000,9999)),
                                'password': 'testpassword',
                                'server_address': 'localhost',
                                'port': '999'
                                },
                        'callme': {
                                   'source_path': 'srcpath',
                                   'destination_path': 'testpath',
                                   },
                        'http': {
                                 'port': '5000'
                                 }
                        }
        
        self._file_content = ''
        
        for section in self._variable_content:
            self._file_content += '[' + section + ']\n'
            for key in self._variable_content[section]:
                self._file_content += key + '=' + self._variable_content[section][key] +'\n'
            self._file_content += '\n'
        
        test_config_file = io.FileIO(self._config_file_name, mode='w', closefd=True)    
        test_config_file.write(str(self._file_content))
        test_config_file.close()

    def testReadConf(self):
        conf.readConf(self._config_file_name)
        self.assertEqual(conf.xivo_cti_http_config, self._variable_content)  

    def removeOldTestConfFiles(self):
        self.assertGreater(len(self.FILE_ROOT), 10, "to protect misuse of remove all files")
        allfiles = glob.glob(self.FILE_ROOT + '*')
        for test_config_file in allfiles:
            try:
                os.remove(test_config_file)
            except OSError as osError:
                if osError.errno != 2:
                    raise osError
