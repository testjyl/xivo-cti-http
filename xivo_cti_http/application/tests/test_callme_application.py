'''
Created on 2012-09-21

@author: jylebleu
'''
import unittest
from xivo_cti_http.application.callme_application import CallMeApplication
from xivo_cti_http.application.call_me_content_builder import CallMeContentBuilder
from mock import Mock
from xivo_cti_http.application.file_writer import FileWriter


class TestCallMe(unittest.TestCase):

    def setUp(self):
        self._writer = Mock(FileWriter)
        self._call_me_content_builder = Mock(CallMeContentBuilder)
        self._call_me_application = CallMeApplication(self._call_me_content_builder, self._writer)
        self._call_me_application.set_dest_path("./spool")

    def test_process(self):
        query = {'id_ars': ['5278'], 'queue_nb': ['5006'], 'nb': ['0298143388'], 'id': ['456']}

        self._call_me_content_builder.build.return_value = 'file content'

        self._call_me_application.process(query)

        self._call_me_content_builder.build.assert_called_with('456', '5278', '5006', '0298143388')
        self._writer.write.assert_called_with('456', 'file content', './spool')
