# -*- coding: UTF-8 -*-

# Copyright (C) 2012  Avencall
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA..
import unittest
from xivo_cti_http.http_server import HttpServer
from mock import Mock
from xivo_cti_http.services.queue_stat_service import QueueStatService
from xivo_cti_http.application.callme_application import CallMeApplication


class TestHttpServer(unittest.TestCase):

    def setUp(self):
        self.queue_stat_service = Mock(QueueStatService)
        self.callme_application = Mock(CallMeApplication)
        self.http_server = HttpServer(5999, self.queue_stat_service, self.callme_application)

    def tearDown(self):
        pass

    def test_request_with_queue_number(self):

        queue_number = '4567'
        start_response = Mock()
        self.queue_stat_service.get_ewt_from_number.return_value = 45

        status = '200 OK'
        headers = [
            ('Content-Type', 'application/json')
        ]

        body = self.http_server.application({'QUERY_STRING': '' + queue_number + '', 'PATH_INFO': '/ewt/'}, start_response)

        start_response.assert_called_with(status, headers)
        self.queue_stat_service.get_ewt_from_number.assert_called_with(queue_number)
        self.assertEquals(body, '{"EWT": 45}')

    def test_request_callback(self):
        start_response = Mock()
        self.callme_application.process.return_value = '{"status": "OK"}'

        status = '200 OK'
        headers = [
            ('Content-Type', 'application/json')
        ]
        request = {'PATH_INFO': '/callme/', 'QUERY_STRING': 'id=456&id_ars=5278&queue_nb=5006&nb=0298143388'}
        body = self.http_server.application(request, start_response)

        start_response.assert_called_with(status, headers)
        self.callme_application.process.assert_called_with({'id_ars': ['5278'], 'queue_nb': ['5006'], 'nb': ['0298143388'], 'id': ['456']})
        self.assertEquals(body, '{"status": "OK"}')
