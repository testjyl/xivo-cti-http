# -*- coding: UTF-8 -*-

# Copyright (C) 2012  Avencall
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA..
import unittest
from xivo_cti_http.cti_client import CtiClient
from mock import Mock

mock_on_test_message = Mock()


class TestCtiClient(unittest.TestCase):

    def setUp(self):
        self._cti_client = CtiClient("localhost", 50030)
        self._on_test_message = Mock()

    def test_register(self):
        message = {"class": "test_message_class"}
        self._cti_client.register(self._on_test_message, "test_message_class")

        self._cti_client.on_message_received(message)

        self._on_test_message.assert_called_with(message)

    def test_on_unknown_message_received(self):
        message = {"class": "unexisting_class"}
        self._cti_client.on_message_received(message)
        self.assertTrue(True, "Message should be ignored")
