#!/usr/bin/env python
# -*- coding: utf-8 -*-

from distutils.core import setup

setup(
    name='xivo-cti-http',
    version='1.0',
    description='XIVO cti http web services',
    author='Avencall',
    author_email='jylebleu@avencall.com',
    url='http://www.avencall.com/',
    license='GPLv3',
    packages=['xivo_cti_http',
              'xivo_cti_http.application',
              'xivo_cti_http.bin',
              'xivo_cti_http.services'
              ],
    scripts=['bin/xivo-cti-http'],
    data_files=[('/etc', ['etc/xivo-cti-http.conf']),
                ('/etc/init.d', ['etc/init.d/xivo-cti-http'])],
)
